export default function (imageFrame, rgbaBuffer) {
  if (imageFrame === undefined) {
    throw new Error('decodeRGB: rgbBuffer must not be undefined');
  }

  let numPixels = 0;

  if (imageFrame.length % 3 === 0) {
    numPixels = imageFrame.length / 3;
  } else if (imageFrame.length % 4 === 0) {
    numPixels = imageFrame.length / 4;
  } else {
    throw new Error('decodeRGB: rgbBuffer length must be divisible by 3');
  }

  let rgbIndex = 0;

  let rgbaIndex = 0;

  for (let i = 0; i < numPixels; i++) {
    rgbaBuffer[rgbaIndex++] = imageFrame[rgbIndex++]; // red
    rgbaBuffer[rgbaIndex++] = imageFrame[rgbIndex++]; // green
    rgbaBuffer[rgbaIndex++] = imageFrame[rgbIndex++]; // blue
    rgbaBuffer[rgbaIndex++] = 255; // alpha
  }
}
